import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'file_uri_provider_method_channel.dart';

abstract class FileUriProviderPlatform extends PlatformInterface {
  /// Constructs a FileUriProviderPlatform.
  FileUriProviderPlatform() : super(token: _token);

  static final Object _token = Object();

  static FileUriProviderPlatform _instance = MethodChannelFileUriProvider();

  /// The default instance of [FileUriProviderPlatform] to use.
  ///
  /// Defaults to [MethodChannelFileUriProvider].
  static FileUriProviderPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FileUriProviderPlatform] when
  /// they register themselves.
  static set instance(FileUriProviderPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String> getUriForFile(String path, String authority) {
    throw UnimplementedError('getUriForFile() has not been implemented.');
  }

}
