
import 'file_uri_provider_platform_interface.dart';

class FileUriProvider {
  Future<String> getUriForFile(String path, String authority) {
    return FileUriProviderPlatform.instance.getUriForFile(path, authority);
  }
}
