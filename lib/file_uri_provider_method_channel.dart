import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'file_uri_provider_platform_interface.dart';

/// An implementation of [FileUriProviderPlatform] that uses method channels.
class MethodChannelFileUriProvider extends FileUriProviderPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('file_uri_provider');


  @override
  Future<String> getUriForFile(String path, String authority) async {
    final String? uri = await methodChannel.invokeMethod<String>('getUriForFile', {
      'path': path,
      'authority': authority,
    });
    if (uri == null) throw FlutterError('Unable to get URI for file');
    return uri;
  }
}
