import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:file_uri_provider/file_uri_provider_method_channel.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MethodChannelFileUriProvider platform = MethodChannelFileUriProvider();
  const MethodChannel channel = MethodChannel('file_uri_provider');

  setUp(() {
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(
      channel,
      (MethodCall methodCall) async {
        return 'mock_uri';
      },
    );
  });

  tearDown(() {
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(channel, null);
  });

  test('getUriForFile', () async {
    const String testPath = 'test/path';
    const String testAuthority = 'test.authority';
    expect(await platform.getUriForFile(testPath, testAuthority), 'mock_uri');
  });
}
