import 'package:flutter_test/flutter_test.dart';
import 'package:file_uri_provider/file_uri_provider.dart';
import 'package:file_uri_provider/file_uri_provider_platform_interface.dart';
import 'package:file_uri_provider/file_uri_provider_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFileUriProviderPlatform
    with MockPlatformInterfaceMixin
    implements FileUriProviderPlatform {


  @override
  Future<String> getUriForFile(String path, String authority) => Future.value('mock_uri');
}

void main() {
  final FileUriProviderPlatform initialPlatform = FileUriProviderPlatform.instance;

  test('$MethodChannelFileUriProvider is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFileUriProvider>());
  });

  test('getUriForFile', () async {
    FileUriProvider fileUriProviderPlugin = FileUriProvider();
    MockFileUriProviderPlatform fakePlatform = MockFileUriProviderPlatform();
    FileUriProviderPlatform.instance = fakePlatform;

    const String testPath = 'test/path';
    const String testAuthority = 'test.authority';

    // Ensure the mock returns the expected mock URI.
    final String result = await fileUriProviderPlugin.getUriForFile(testPath, testAuthority);
    expect(result, 'mock_uri');
  });
}
