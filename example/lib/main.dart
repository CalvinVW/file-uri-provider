import 'package:flutter/material.dart';
import 'package:file_uri_provider/file_uri_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  String _fileUri = '';

  void _getFileUri() async {
    // Replace with a valid file path and authority for your app
    const String filePath = '/path/to/file';
    const String fileProviderAuthority = 'com.yourdomain.yourapp.fileprovider';

    try {
      String fileUri = await FileUriProvider().getUriForFile(filePath, fileProviderAuthority);
      setState(() {
        _fileUri = fileUri;
      });
    } catch (e) {
      print('Failed to get file URI: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('File Uri Provider Example'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: _getFileUri,
              child: const Text('Get File URI'),
            ),
            const SizedBox(height: 20),
            Text(_fileUri.isEmpty ? 'Press the button to get the file URI' : 'File URI: $_fileUri'),
          ],
        ),
      ),
    );
  }
}
