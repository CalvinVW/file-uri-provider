# File Uri Provider Plugin

A Flutter plugin for obtaining a file URI using the Android FileProvider.

## Features

- Fetch file URIs securely for Android platform.
- Support for background and foreground usage.

## Usage

To use this plugin, add `file_uri_provider` as a dependency in your `pubspec.yaml` file.

### Example

```dart
import 'package:file_uri_provider/file_uri_provider.dart';

void main() async {
  const String filePath = 'path/to/your/file';
  const String fileProviderAuthority = 'com.example.app.fileprovider';
  
  try {
    final String fileUri = await FileUriProvider.getUriForFile(filePath, fileProviderAuthority);
    print('File URI: $fileUri');
  } catch (e) {
    print('Failed to get file URI: $e');
  }
}
