package com.pageme.file_uri_provider

import androidx.annotation.NonNull
import android.net.Uri
import java.io.File
import android.content.Context
import androidx.core.content.FileProvider
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** FileUriProviderPlugin */
class FileUriProviderPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var context: Context // Define a property to hold the context

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "file_uri_provider")
    context = flutterPluginBinding.applicationContext // Store the application context in the property
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "getUriForFile" -> {
        val path = call.argument<String>("path")
        val authority = call.argument<String>("authority")
        if (path != null && authority != null) {
          try {
            // Use the stored context property here
            val uri = getUriForFile(context, path, authority)
            result.success(uri.toString())
          } catch (e: Exception) {
            result.error("URI_ERROR", "Error creating file URI", null)
          }
        } else {
          result.error("INVALID_ARGUMENTS", "Invalid or missing arguments", null)
        }
      }
      "getPlatformVersion" -> {
        result.success("Android ${android.os.Build.VERSION.RELEASE}")
      }
      else -> {
        result.notImplemented()
      }
    }
  }

  private fun getUriForFile(context: Context, path: String, authority: String): Uri {
    val file = File(path)
    return FileProvider.getUriForFile(context, authority, file)
  }


  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
